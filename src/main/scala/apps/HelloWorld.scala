package apps

/**
  * Created by mark on 16/04/2017.
  */
object HelloWorld extends App{
  print("Enter your name")
  val somebody=readLine()
  println(s"Hello$somebody!!")
}


// extends App 拿掉就不會有 run
// 透過 terminal 執行：sbt run
//scala-cp target/scala-2.10/scala4thu-c6_2.10-1.0.jar : -cp 是因為有多個可執行，須選擇執行哪個

